
// SECTION 3 TABS START
function rudrSwitchTab(rudr_tab_id, rudr_tab_content) {
    let x = document.getElementsByClassName("section-3-tabs-container");
    let i;
    for (i = 0; i < x.length; i++) {
        x[i].style.display = 'none';
    }
    document.getElementById(rudr_tab_content).style.display = 'block';

    x = document.getElementsByClassName("section-3-tabs-button");
    for (i = 0; i < x.length; i++) {
        x[i].className = 'section-3-tabs-button';
    }
    document.getElementById(rudr_tab_id).className = 'section-3-tabs-button section-3-tabs-button-active';
}
// SECTION 3 TABS END


// SECTION 5 FILTER START
filterSelection("all")
function filterSelection(c) {
    let x, i;
    x = document.getElementsByClassName("filterDiv");
    if (c === "all") c = "";
    for (i = 0; i < x.length; i++) {
        removeClass(x[i], "show");
        if (x[i].className.indexOf(c) > -1) addClass(x[i], "show");
    }
}

function addClass(element, name) {
    let i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        if (arr1.indexOf(arr2[i]) === -1) {
            element.className += " " + arr2[i];
        }
    }
}

function removeClass(element, name) {
    let i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);
        }
    }
    element.className = arr1.join(" ");
}

let btnContainer = document.getElementById("section-5-tabs");
let btns = btnContainer.getElementsByClassName("section-5-filter-tab");
for (let i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function() {
        let current = document.getElementsByClassName("active");
        current[0].className = current[0].className.replace(" active", "");
        this.className += " active";
    });
}
// SECTION 5 FILTER END


// CAROUSEL START
// $('.carousel-bar-photos').on('click', function () {
//     counterId = +this.id.slice(-1);
//     document.getElementById('start-photo').style.border = '';
//     $('.carousel-bar-photos').css('margin-bottom', '0px');
//     $('#${this.id}').css('margin-bottom', '30px');
//     $('.testimonial').css('display', 'none');
//     $('#${this.id}-testimonial').css('display', 'block');
//     $('.carousel-name').css('display', 'none');
//     $('#${this.id}-name').css('display', 'block');
//     $('.carousel-position').css('display', 'none');
//     $('#${this.id}-position').css('display', 'block');
//     $('.carousel-main-photo-selected').fadeOut(500, () => {
//         document.getElementById('testimonial-selected').style.backgroundImage =
//             'url('./img/section-7-testimonials-${this.id}.png')';
//         $('.carousel-main-photo-selected').fadeIn(500);
//     });
// });
//
// function change() {
//     document.getElementById('start-photo').style.border = '';
//     $()
// }

// MASONRY START
let grid = document.querySelector(".grid");
new TinyMasonry(grid);
// MASONRY END


